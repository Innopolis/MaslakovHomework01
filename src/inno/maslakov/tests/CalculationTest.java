import inno.maslakov.calc.Calculation;
import org.junit.jupiter.api.Test;

import java.math.BigInteger;
import java.util.LinkedList;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by sa on 13.02.17.
 */
class CalculationTest {
    @Test
    void getSumNormal() {
        LinkedList<Integer> list = new LinkedList<>();
        for(int i = 0; i < 10; i++){
            list.add(i);
        }
        Calculation calcNumber = new Calculation(list);
        BigInteger sum = calcNumber.getSum();
        assertEquals(sum, BigInteger.valueOf(45));
    }

    @Test
    public void testCalculationNegative(){
        LinkedList<Integer> list = new LinkedList<>();
        for(int i = 0; i < 10; i++){
            list.add(-10);
        }
        Calculation calculation = new Calculation(list);
        BigInteger sum = calculation.getSum();
        assertEquals(sum, BigInteger.valueOf(-100));
    }


}