import inno.maslakov.ThreadCalcSum;
import org.junit.jupiter.api.Test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigInteger;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by sa on 13.02.17.
 */
class ThreadCalcSumTest {
    @Test
    void getSum() throws IOException, InterruptedException {
        String[] resources = new String[5];
        String fileName = "testThreadCalcSum.txt";
        File file=new File(fileName);
        file.createNewFile();
        BufferedWriter bw = new BufferedWriter(new FileWriter(file));
        for(int i = -5; i < 5; i++){
            bw.write(i + " ");
        }
        bw.close();
        for (int i = 0; i < resources.length; i++){
            resources[i] = fileName;
        }
        resources[1] = "https://gitlab.com/Innopolis/MaslakovHomework01/raw/" +
                "feature/ExceptionCorrect/urlFile.txt";

        ThreadCalcSum threadCalcSum = new ThreadCalcSum(resources);
        BigInteger sum = threadCalcSum.getSum();
        file.delete();

        assertEquals(sum, BigInteger.valueOf(34));
    }

    @Test
    public void testGetSumBadFile() throws IOException {
        String[] resources = new String[5];
        String fileName = "testThreadCalcSum.txt";
        String badFileName = "testThreadCalcSumBadFile.txt";

        File file=new File(fileName);
        File badFile = new File(badFileName);
        file.createNewFile();
        badFile.createNewFile();
        BufferedWriter bw = new BufferedWriter(new FileWriter(file));
        BufferedWriter bwBad = new BufferedWriter((new FileWriter(badFile)));
        for(int i = -5; i < 5; i++){
            bw.write(i + " ");
            bwBad.write(i + " ");
            if (i == 0){
                bwBad.write("bad");
            }
        }
        bw.close();
        bwBad.close();
        for (int i = 0; i < resources.length; i++){
            resources[i] = fileName;
        }
        resources[1] = "https://gitlab.com/Innopolis/MaslakovHomework01/raw/" +
                "feature/ExceptionCorrect/urlFile.txt";
        resources[2] = badFileName;

        ThreadCalcSum threadCalcSum = new ThreadCalcSum(resources);

        assertThrows(IOException.class, () -> threadCalcSum.getSum());

        file.delete();
        badFile.delete();

    }

    @Test
    public void testGetSumBadURL(){
        String[] resources = new String[5];
        for (int i = 0; i < resources.length; i++){
            resources[i] = "https://gitlab.com/Innopolis/MaslakovHomework01/raw/" +
                    "feature/ExceptionCorrect/urlFile.txt";
        }
        resources[2] = "https://gitlab.com/DontKnowIt/urlFile.fileNotFound";

        ThreadCalcSum threadCalcSum = new ThreadCalcSum(resources);

        assertThrows(IOException.class, ()->threadCalcSum.getSum());
    }

}