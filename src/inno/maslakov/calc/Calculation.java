package inno.maslakov.calc;

import java.math.BigInteger;
import java.util.LinkedList;

/**
 * Created by sa on 07.02.17.
 */

/**
 * Класс предназначен для вычисления суммы переданных параметров.
 * Класс работает со связным списком
 */
public class Calculation {
    private final LinkedList<Integer> list;

    /**
     * Конструктор класса вычислителя.
     * @param list связной список входных данных
     */
    public Calculation(LinkedList<Integer> list) {
        this.list = list;
    }

    /**
     * Метод расчета суммы переданного связного списка в конструкторе.
     * @return вычисленную сумму всех элементов в типе BigInteger
     */
    public BigInteger getSum () {
        BigInteger sum = BigInteger.valueOf(0);
        for (Integer num:
                list) {
            sum = sum.add(BigInteger.valueOf(num));
            System.out.println("local sum = " + sum);
        }
        return sum;
    }

}
