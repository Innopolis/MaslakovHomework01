package inno.maslakov;

import inno.maslakov.DataProcessing.DataReader;
import inno.maslakov.DataProcessing.ParseInput;
import inno.maslakov.calc.Calculation;

import java.io.IOException;
import java.math.BigInteger;
import java.util.*;

/**
 * Created by sa on 09.02.17.
 */

/**
 * Класс, запускающий потоки для вычисления суммы всех переданных ресурсов
 */
@SuppressWarnings("ALL")
public class ThreadCalcSum implements Runnable{
    private final String[] resources;
    private volatile BigInteger sum = BigInteger.valueOf(0);
    private volatile boolean flagStopError = false;
    private final ArrayList<Thread> threads;
    private final PriorityQueue<String> fileNames;

    /**
     * Конструктор инициализирующий внутеннее поле ресурсов
     * @param resources список ресурсов, которые требуется просуммировать
     */
    @SuppressWarnings("unchecked")
    public ThreadCalcSum(String[] resources) {
        this.resources = resources;
        //noinspection unchecked
        threads = new ArrayList(resources.length);
        fileNames = new PriorityQueue<>(resources.length);
    }

    /**
     * Метод обработки одного потока для одного ресурса
     * @param name имя ресурса, который требуется обработать
     * @throws IOException исключения при работе с файлами (отсутствие)
     */
    private void reader(String name) throws IOException {
        DataReader dataReader = new DataReader(name);
        LinkedList<Integer> list = dataReader.read();
        ParseInput parseInput = new ParseInput(list);
        list = parseInput.findEvenPositive();
        Calculation calculation = new Calculation(list);
        BigInteger tmpSum = calculation.getSum();
        synchronized (sum) {
            System.out.println("Sum = " + sum);
            sum = sum.add(tmpSum);
        }
    }

    /**
     * Метод вычисляющий общую сумму по всем потокам
     * @return Вычисленная сумма по всем ресурсам
     * @throws IOException ошибки связанные с работой с файлами
     */
    public BigInteger getSum() throws InterruptedException, IOException {
        for (String name :
                resources) {
            if (flagStopError) {
                break;
            }
            fileNames.add(name);
            Thread thread = new Thread(this);
            threads.add(thread);
            thread.start();
        }

        /*Ожидание завершения всех потоков*/
        for (Thread th :
                threads) {
            try {
                th.join();
            } catch (InterruptedException e) {
                throw e;
            }
        }
        if (flagStopError){
            throw new IOException("Ошибка во входных данных");
        }
        return sum;
    }

    @Override
    public void run() {
        try {
            reader(fileNames.poll());
        } catch (InputMismatchException ex) {
            System.out.println("bad input");
            flagStopError = true;
        } catch (IOException e) {
            System.out.println("file not found");
            flagStopError = true;
        }
    }
}
