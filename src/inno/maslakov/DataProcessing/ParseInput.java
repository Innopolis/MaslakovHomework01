package inno.maslakov.DataProcessing;

import java.util.LinkedList;

/**
 * Created by sa on 07.02.17.
 */

/**
 * Класс - парсер связного списка
 */
public class ParseInput {
    private final LinkedList<Integer> inputList;

    /**
     * Конструктор класса инициализирует внутренний связный список
     * @param inputList - принимает набор данных, которые
     *                  необходимо распарсить
     */
    public ParseInput(LinkedList<Integer> inputList) {
        this.inputList = inputList;
    }

    /**
     * Метод поиска четных и положительных чисел
     * @return Связной список положительных и четных чисел
     */
    public LinkedList<Integer> findEvenPositive(){
        LinkedList<Integer> list= new LinkedList<>();
        for (Integer num:
             inputList) {
            if ((num > 0) && (num % 2 == 0)){
                list.add(num);
            }
        }
        return list;
    }

}
