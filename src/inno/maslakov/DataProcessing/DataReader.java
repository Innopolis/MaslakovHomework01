package inno.maslakov.DataProcessing;

import java.io.*;
import java.net.URL;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by sa on 08.02.17.
 */

/**
 * Класс предназначенный для чтения входных ресурсов (файлы и url) их
 * аккумулирования в связный список и выдаче их по требованию
 */
public class DataReader {
    private final String resource;

    /**
     * @param resource принимает имя ресурса, с которым планируется работа
     */
    public DataReader(String resource) {
        this.resource = resource;
    }

    /**
     * Метод чтения ресурса. Определяет тип ресурса и вызывает соответствующий
     * метод.
     * @return Связной список прочитанных целочисленных данных
     * @throws IOException при отстутствии файла, или иными проблемами с ним
     * выбрасывает исключение
     */
    public LinkedList<Integer> read() throws IOException {
        LinkedList<Integer> list;
        if (isURL(resource)){
            System.out.println("start read URL");
            list = readURL();
        }
        else{
            System.out.println("start read File");
            list = readFile();
        }
        return list;
    }

    /**
     * Метод чтения локального файла
     * @return Связной список прочитанных целочисленных данных
     * @throws IOException при отстутствии файла, или иными проблемами с ним
     * выбрасывает исключение
     */
    private LinkedList<Integer> readFile() throws IOException {
        LinkedList<Integer> list= new LinkedList<>();

        try (FileInputStream fis = new FileInputStream(resource);
             Reader reader = new InputStreamReader(fis, "UTF-8");
             Scanner scanner = new Scanner(reader)) {

            while (scanner.hasNext()) {
                int value = scanner.nextInt();
                list.add(value);
            }
        }
        return list;
    }

    /**
     * Метод чтения удаленного файла
     * @return Связной список прочитанных целочисленных данных
     * @throws IOException при отстутствии файла, или иными проблемами с ним
     * выбрасывает исключение
     */
    private LinkedList<Integer> readURL() throws IOException {
        LinkedList<Integer> list = new LinkedList<>();
        URL url = new URL(resource);
        try(Scanner scanner = new Scanner(url.openStream())){
            while(scanner.hasNextInt()){
                int value = scanner.nextInt();
                list.add(value);
            }
        }

     return list;
    }

    /**
     * Метод проверки URL. Проверка ведется по маске
     * @param name Имя удаленного ресурса
     * @return true - если имя соответствет потенциальному имени URL
     */
    private boolean isURL (String name){
        Pattern p = Pattern.compile("[Hh][Tt][Tt][Pp][Ss]?://\\w+.");
        Matcher m = p.matcher(name);
        return m.find();
    }
}
