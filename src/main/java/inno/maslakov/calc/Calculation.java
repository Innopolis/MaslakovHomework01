package inno.maslakov.calc;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import java.math.BigInteger;
import java.util.LinkedList;

/**
 * Created by sa on 07.02.17.
 */

/**
 * Класс предназначен для вычисления суммы переданных параметров.
 * Класс работает со связным списком
 */
public class Calculation {
    private final LinkedList<Integer> list;
    private Logger logger = Logger.getLogger(Calculation.class);
    static {
        DOMConfigurator.configure("src/main/resources/log4j.xml");
    }

    /**
     * Конструктор класса вычислителя.
     * @param list связной список входных данных
     */
    public Calculation(LinkedList<Integer> list) {
        this.list = list;
    }

    /**
     * Метод расчета суммы переданного связного списка в конструкторе.
     * @return вычисленную сумму всех элементов в типе BigInteger
     */
    public BigInteger getSum () {
        BigInteger sum = BigInteger.valueOf(0);
        for (Integer num:
                list) {
            sum = sum.add(BigInteger.valueOf(num));
            logger.info("SUM = " + sum);
        }
        return sum;
    }

}
