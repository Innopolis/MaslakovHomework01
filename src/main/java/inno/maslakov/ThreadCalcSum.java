package inno.maslakov;

import inno.maslakov.DataProcessing.DataReader;
import inno.maslakov.DataProcessing.ParseInput;
import inno.maslakov.calc.Calculation;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import java.io.IOException;
import java.math.BigInteger;
import java.util.*;

/**
 * Created by sa on 09.02.17.
 */

/**
 * Класс, запускающий потоки для вычисления суммы всех переданных ресурсов
 */
@SuppressWarnings("ALL")
public class ThreadCalcSum implements Runnable{
    private final String[] resources;
    private volatile BigInteger sum = BigInteger.valueOf(0);
    private volatile boolean flagStopError = false;
    private final ArrayList<Thread> threads;
    private final PriorityQueue<String> fileNames;
    private Logger logger = Logger.getLogger(ThreadCalcSum.class);
    static {
        DOMConfigurator.configure("src/main/resources/log4j.xml");
    }

    /**
     * Конструктор инициализирующий внутеннее поле ресурсов
     * @param resources список ресурсов, которые требуется просуммировать
     */
    @SuppressWarnings("unchecked")
    public ThreadCalcSum(String[] resources) {
        this.resources = resources;
        //noinspection unchecked
        threads = new ArrayList(resources.length);
        fileNames = new PriorityQueue<>(resources.length);
    }

    /**
     * Метод обработки одного потока для одного ресурса
     * @param name имя ресурса, который требуется обработать
     * @throws IOException исключения при работе с файлами (отсутствие)
     */
    private void reader(String name) throws IOException {
        DataReader dataReader = new DataReader(name);
        LinkedList<Integer> list = dataReader.read();
        if (!flagStopError) {
            ParseInput parseInput = new ParseInput(list);
            list = parseInput.findEvenPositive();
            Calculation calculation = new Calculation(list);
            BigInteger tmpSum = calculation.getSum();
            synchronized (sum) {
                sum = sum.add(tmpSum);
                logger.info("GrobalSum = " + sum);//
            }
        }
    }

    /**
     * Метод вычисляющий общую сумму по всем потокам
     * @return Вычисленная сумма по всем ресурсам
     * @throws IOException ошибки связанные с работой с файлами
     */
    public BigInteger getSum() throws Exception {
        for (String name :
                resources) {
            if (flagStopError) {
                break;
            }
            fileNames.add(name);
            Thread thread = new Thread(this);
            threads.add(thread);
            thread.start();
        }

        /*Ожидание завершения всех потоков*/
        for (Thread th :
                threads) {
            try {
                th.join();
            } catch (InterruptedException e) {
                logger.error("Остановка с помощью Interrupted: " + e.getMessage());
                e.printStackTrace();
            }
        }
        if (flagStopError){
            logger.error("Ошибка входных данных");
            throw new Exception("Ошибка во входных данных");
        }
        logger.info("FINAL = " + sum);
        return sum;
    }

    @Override
    public void run() {
        try {
            reader(fileNames.poll());
        } catch (InputMismatchException e) {
            logger.error("Обнаружено нечисловое значение");
            flagStopError = true;
        } catch (IOException e) {
            logger.error("Файл не найден: " + e.getMessage());
            flagStopError = true;
        }
    }
}
