import inno.maslakov.ThreadCalcSum;

import java.io.File;
import java.io.IOException;


/**
 * Created by sa on 07.02.17.
 */
class Main {
    public static void main (String[] args){
        ThreadCalcSum threadReader = new ThreadCalcSum(args);

        new File("logProgram.log").delete();
        try {
            System.out.println("CalcSum = " + threadReader.getSum());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }
}
