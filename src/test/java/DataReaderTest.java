import inno.maslakov.DataProcessing.DataReader;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by sa on 13.02.17.
 */
class DataReaderTest {
    @Test
    void read() throws IOException {
        String res = "testReadFile.txt";
        File file=new File(res);
        file.createNewFile();
        BufferedWriter bw = new BufferedWriter(new FileWriter(file));
        LinkedList <Integer> inputList = new LinkedList<>();
        for(int i = -5; i < 5; i++){
            bw.write(i + " ");
            inputList.add(i);
        }
        bw.close();
        DataReader dataReader = new DataReader(res);
        LinkedList<Integer> readList = dataReader.read();
        file.delete();
        assertEquals(readList, inputList);
    }

    @Test
    public void testReadFileNotFound(){
        String res = "MaslakovNotFoundFile.notFound";
        DataReader dataReader = new DataReader(res);

        assertThrows(Exception.class, ()->dataReader.read());
    }

    public void testReadURL() throws IOException {
        String res = "https://gitlab.com/Innopolis/MaslakovHomework01/raw/" +
                "feature/ExceptionCorrect/urlFile.txt";
        DataReader dataReader = new DataReader(res);
        LinkedList<Integer> listIn = dataReader.read();
        LinkedList<Integer> listExpect = new LinkedList<>();

        for(int i = 0; i < 5; i++) {
            listExpect.add(-1);
            listExpect.add(-2);
            listExpect.add(-3);
            for (int j = 0; j < 4; j++) {
                listExpect.add(j);
            }
        }
        assertEquals(listIn, listExpect);
    }

    @Test
    public void testReadURLNotFound() throws IOException {
        String res = "https://FileNotFound.com/urlFile.txt";
        DataReader dataReader = new DataReader(res);

        assertThrows(IOException.class, () -> dataReader.read());
    }

}