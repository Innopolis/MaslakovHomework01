import inno.maslakov.DataProcessing.ParseInput;
import org.junit.jupiter.api.Test;

import java.util.LinkedList;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by sa on 13.02.17.
 */
class ParseInputTest {
    @Test
    void findEvenPositive() {
        LinkedList<Integer> list = new LinkedList<>();
        for(int i = -5; i < 5; i++){
            list.add(i);
        }
        ParseInput parseInput = new ParseInput(list);
        list = parseInput.findEvenPositive();
        LinkedList<Integer> listConst = new LinkedList<>();
        listConst.add(2);
        listConst.add(4);

        assertEquals(list, listConst);
    }

}